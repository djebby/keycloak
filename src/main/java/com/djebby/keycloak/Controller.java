package com.djebby.keycloak;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/keycloak")
public class Controller {

    @GetMapping
    @PreAuthorize("hasRole('client_user')")
    public String keycloak() {
        return "if you get this response message that's means that keycloak authenticate you.";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('client_admin')")
    public String keycloakAdmin() {
        return "if you get this response message that's means that keycloak authorize you as admin.";
    }

}
































